#!/usr/bin/env ruby 
#

require "tk" 
require_relative "./lib/roi.rb"

help = <<~help 
#
# select-rois 
#
# Este programa permite seleccionar las regiones de interés en una 
# secuencia de imágenes en formato "pnm". 
#
# Uso: 
#   find [image-dir] -type 'f' | ./rois-select.rb - 
#   ls [image-dir]/* | ./select-rois.rb - 
#   ./select-rois.rb - < [lista-imagenes.txt]
#
# Opciones: 
#   --nrois=  | -n=  número de regiones a seleccionar  
#   --verbose | -v   imprimir mas información (notas)
#   --help    | -h   desplegar esta información
#
# Entrada: 
#   Lee de STDIN los nombres de los archivos de imágenes en formato pgm
#
#   Formato:
#     # Comentario y lineas en blanco son ignoradas
#     Archivo-1.pgm
#     Archivo-2.pgm
#
# Salida: 
#   Escribe a SDTOUT los nombres de los archivos y su respectivas regiones 
#   de interés
#   
#   Formato: 
#     # Notas 
#     => Nombre-de-archivo-1.pgm
#     x1 y1 x2 y2
#     x1 y1 x2 y2
#     x1 y1 x2 y2
#     => nombre-de-archivo-2.pgm
#     x1 y1 x2 y2
#     x1 y1 x2 y2
#     x1 y1 x2 y2
# 
help

# Config  ---------------------------------------------------------------------
#
nrois = 5
verbose = false


# Config Parsing --------------------------------------------------------------
#
if ARGV.empty? || ARGV.any?{|a| a =~ /^--help$|^-h$/} || !ARGV.include?("-") 
  puts help 
  exit 0
end 

if k = ARGV.find_index{|a| a =~ /--?n(rois)?=?/}
  if ARGV[k] =~ /=/
    nrois = ARGV[k].split(/=/)[-1].to_i
  else
    nrois = ARGV[k+1].to_i 
  end 
end 

if k = ARGV.find_index{|a| a =~ /(--verbose|-v)/}
  verbose = true 
end 

# Programa ====================================================================
#

# Cargar imagenes ---
file_names = [] 
STDIN.each_line do |line| 
  next if line =~ /(^\s*#.\*$|^\s*$)/
  file_names.push line.chop.strip
end 
file_names = file_names.sort!
images = file_names.map do |filename|
  puts "# Loading image #{File.basename filename}" if verbose 
  TkPhotoImage.new(file: filename) 
end 
sorted_indexes = (0 ... file_names.size).sort_by{|i| file_names[i]}
file_names     = sorted_indexes.map{|i| file_names[i]}
images         = sorted_indexes.map{|i| images[i]}

action = :none
img_rois = Array.new(images.size){[]} 

# GUI -------------------------------------------------------------------------
window_size = 500 

# Ventana Principal 
root = TkRoot.new 
root["title"] = "select-rois"
root["geometry"] = "#{window_size}x#{window_size}"
TkGrid.columnconfigure root, 0, weight: 1
TkGrid.rowconfigure    root, 0, weight: 1

# Crear canvas para mostrar las imágenes y rois
canvas = TkCanvas.new(root)
canvas["relief"] = "s"
canvas.grid sticky: "nwes", column: 0, row: 0
canvas.width =  window_size
canvas.height = window_size
canvas.focus

sx =  (canvas.width.to_f / images[0].width).round(0)
sy =  (canvas.height.to_f / images[0].height).round(0)
zoom =TkPhotoImage.new
canvas_im_obj = TkcImage.new(canvas,0,0)


# Progress Bar 
img_cnt = -1
pb = Tk::Tile::Progressbar.new(root)
pb["orient"] = "horizontal"
pb["length"] = canvas.width
pb["maximum"] = images.size - 1
pb["value"] = 0
pb.grid column: 0, row: 2, sticky: "w"


export_rois = -> do 
  puts "# exporting rois" if verbose
  (0 ... images.size).each do |i| 
    filename = file_names[i] 
    rois = img_rois[i]
    puts "=> #{filename}"
    (0 ... rois.size).each do |r| 
      x1,y1,x2,y2 = rois[r].coords
      x1 /= sx 
      x2 /= sx 
      y1 /= sy
      y2 /= sy
      puts "#{x1} #{y1} #{x2} #{y2}"
    end 
  end 
  Tk.exit
end 


update_subsequent_rois = ->(roi) do 
  puts "# updating subsequent rois" if verbose
  color = roi.color
  (img_cnt+1... img_rois.size).each do |i|
    rois = img_rois[i]
    if i = rois.find_index{|r| r.color == color } 
      roidup = roi.dup
      roidup.update = false
      rois[i] = roidup
    end 
  end 
end 

update_all_rois = ->(roi) do 
  puts "# updating all rois" if verbose
  color = roi.color
  (0 ... img_rois.size).each do |i|
    next if i == img_cnt
    rois = img_rois[i]
    if i = rois.find_index{|r| r.color == color } 
      roidup = roi.dup
      roidup.update = false
      rois[i] = roidup
    end 
  end 
end 

move_roi = ->(xi,yi) do 
  rois = img_rois[img_cnt]
  if (roi = rois.select{|r| r.update}.pop) && roi.update
    puts "# moving roi" if verbose
    update_roi_pos = ->(xf,yf) do 
      dx = xf - xi 
      dy = yf - yi 
      roi.move(dx,dy)
      xi = xf 
      yi = yf
    end 
    canvas.bind("B1-Motion",update_roi_pos,"%x %y")
    update_subsequent_rois.call roi
  end 
end 

draw_new_roi = ->(xi,yi) do 
  if img_rois[img_cnt].size < nrois 
    puts "# new roi" if verbose
    roi = Roi.new(canvas,xi,yi)
    update_roi_size = ->(xf,yf) do 
      roi.coords(xi,yi,xf,yf)
    end 
    canvas.bind("B1-Motion",update_roi_size,"%x %y")
    img_rois.each do |rois| 
      rois.push roi
    end 
  end 
end 

scale_roi = ->(xi,yi) do 
  puts "# scaling roi" if verbose
  
  rois = img_rois[img_cnt]
  if (roi = rois.select{|r| r.update}.pop) && roi.update
    scaling = ->(xf,yf) do 
      x1,y1,x2,y2 = roi.coords
      dx = xf - xi
      dy = yf - yi
      
      cx,cy = roi.center

      roi.coords(x1 + dx,y1 + dy,x2,y2) if xf < cx && yf < cy
      roi.coords(x1 + dx,y1,x2,y2 + dy) if xf < cx && yf > cy
      roi.coords(x1,y1 + dy,x2 + dx,y2) if xf > cx && yf < cy
      roi.coords(x1,y1,x2 + dx,y2 + dy) if xf > cx && yf > cy
        
      xi = xf 
      yi = yf
    end 
    canvas.bind("B1-Motion",scaling,"%x %y")
    update_all_rois.call roi
  end 
end 

select_release_action = ->() do 
  canvas.bind("B1-Motion",->(){})
  action = :none
end 

select_pressed_action = ->(x,y) do 
  case action  
  when :newroi then draw_new_roi.call(x,y)
  when :moveroi then move_roi.call(x,y)
  when :scale   then scale_roi.call(x,y)
  end 
end 

draw_image = -> do 
  im = images[img_cnt]
  zoom.copy(im, zoom: [sx,sy])
  canvas_im_obj.delete 
  canvas_im_obj = TkcImage.new(canvas,0,0,[image: zoom,anchor: "nw"])
  rois = img_rois[img_cnt]
  (0 ... rois.size).each do |i|
    rois[i] = rois[i].draw
  end 
end 

next_image = ->() do 
  if img_cnt < images.size - 1
    img_cnt += 1
    pb.value = img_cnt
    puts "# next image #{File.basename file_names[img_cnt]}" if verbose
    draw_image.call
  end 
end 

prev_image = ->() do 
  if img_cnt > 0 
    img_cnt -= 1
    pb.value = img_cnt
    puts "# prev image #{File.basename file_names[img_cnt]}" if verbose
    draw_image.call
  end 
end 

delete_roi = -> do 
  puts "# delete roi" if verbose

  rois = img_rois[img_cnt]
  if (roi = rois.select{|r| r.update}.pop) && roi.update
    color = roi.color 
    img_rois.each do |rois|
      if i = rois.find_index{|r| r.color == color } 
        rois.delete_at(i)
      end 
    end 
    Roi.colors.push color
    draw_image.call
  end 
end 

roi_toggleshape = -> do 
  puts "# toggle shape" if verbose
  rois = img_rois[img_cnt]
  if (roi = rois.select{|r| r.update}.pop) && roi.update
    color = roi.color 
    roi.toggleshape
    img_rois.each do |rois|
      if i = rois.find_index{|r| r.color == color } 
        rois[i] = roi.dup
      end 
    end 
    draw_image.call
  end 
end 

help = -> do 

  cx = canvas.width / 2
  cy = canvas.height / 2
  x1 = cx - 200 
  y1 = cy - 200 
  x2 = cx + 200 
  y2 = cy + 200 

  rect = TkcRectangle.new(canvas,x1,y1,x2,y2)
  rect["fill"] = "black"
  txt = TkcText.new(canvas,cx,cy)
  txt["justify"] = "center"
  txt["fill"] = "white"
  txt["font"] = TkFont.new(size: 18)
  text = <<~text
  r : Crear una nueva roi
  d : Borrar una roi

  m : Mover una roi
  s : Redimencionar una roi
  t : Cambiar forma
  
  n : Imagen siguiente 
  p : Imagen anterior

  q : Salir del programa

  ? : Esta ayuda
  text
  txt["text"] = text
end 

canvas.bind("KeyPress", 
  ->(key) do 
  case key 
  when "r" then action = :newroi
  when "m" then action = :moveroi
  when "s" then action = :scale
  when "n" then next_image.call
  when "d" then delete_roi.call
  when "t" then roi_toggleshape.call 
  when "p" then prev_image.call
  when "q" then export_rois.call
  when "?" then help.call
  end 
end, "%A"); 


canvas.bind("ButtonPress-1",select_pressed_action, "%x %y")
canvas.bind("ButtonRelease-1",select_release_action)

next_image.call

Tk.mainloop

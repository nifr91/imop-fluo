require "tk" 

class Roi
  @@colors = [
    "#AFEEEE", # paleturquoise     
    "#48D1CC", # mediumturquoise   
    "#00CED1", # darkturquoise     
    "#008B8B", # darkcyan          
    "#FF7F50", # coral      
    "#FF4500", # orangered  
    "#FFD700", # gold       
    #------------------
    "#9e512b", # marrón
    "#7f007f", # magenta oscuro  
    "#ffa300", # naranja 
    "#006300", # verde oscuro
    "#bfbfbf", # gris
    "#00ff00", # verde
    "#ff0000", # rojo 
    "#00ffff", # cyan
    "#ff00ff", # magenta
    "#ffff00" # amarillo
  ]



  attr_accessor :update
  attr_accessor :color

  def initialize(canvas,x,y,x2 = x,y2 = y,shape = :circle, color = @@colors.pop)
    @shape = shape
    @canvas = canvas
    @color = color
    @tkroi = draw_tkroi(x,y,x2,y2)
    @update = false 
  end 

  def toggleshape
    @shape = case @shape 
      when :circle then :rect 
      else :circle 
    end
    redraw 
  end 

  private def draw_tkroi(x,y,x2,y2)
    tkroi = case @shape
    when :circle then TkcOval.new(@canvas,x,y,x2, y2) 
    when :rect  then TkcRectangle.new(@canvas,x,y,x2,y2)
    end 
    tkroi["outline"] = @color
    tkroi["width"] = 3
    tkroi["activewidth"]= 8
    tkroi.bind("Enter",->(){@update = true}) 
    tkroi.bind("Leave",->(){@update = false}) 
    tkroi 
  end 

  def redraw 
    x1,y1,x2,y2 = @tkroi.coords
    @tkroi.delete
    @tkroi = draw_tkroi(x1,y1,x2,y2)
    self 
  end 

  def bind(*args) 
    @tkroi.bind(*args) 
  end 
  def coords(*args)
    @tkroi.coords(*args)
  end 

  def move(*args)
    @tkroi.move(*args)
  end

  def draw
    x1,y1,x2,y2 = coords
    Roi.new(@canvas,x1,y1,x2,y2,@shape,@color)
  end 

  def center
    x1,y1,x2,y2 = @tkroi.coords
    cx = x1 + ((x2 - x1) / 2)
    cy = y1 + ((y2 - y1) / 2)
    [cx,cy]
  end 

  def self.colors
    @@colors
  end 
end 

